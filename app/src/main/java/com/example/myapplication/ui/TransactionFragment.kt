package com.example.myapplication.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.myapplication.adapter.ListTransactionAdapter
import com.example.myapplication.data.Connect
import com.example.myapplication.databinding.FragmentTransactionBinding
import kotlin.concurrent.thread

class TransactionFragment : Fragment() {


    private lateinit var  binding : FragmentTransactionBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTransactionBinding.inflate(layoutInflater)
        getTransaction()
        return binding.root
    }

    fun getTransaction() {
        thread {
            try {
                val data = Connect.getTransaksi()

                if (data.size > 0)
                {
                    Log.d("err-gettransaction", data.size.toString())
                    activity?.runOnUiThread {
                        binding.listTransaction.adapter = ListTransactionAdapter(this, data)
                    }
                } else {
                    Log.d("err-login", data.size.toString())
                    activity?.runOnUiThread {
                        Toast.makeText(activity, "TIdak ada data untuk ditampilkan", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            } catch (ex : Exception) {
                Log.d("err-getTransaction", ex.toString())
            }
        }
    }
}

