package com.example.myapplication.ui

import android.content.Intent
import android.os.Bundle
import android.renderscript.Script.KernelID
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import com.example.myapplication.ConfirmationActivity
import com.example.myapplication.InvoiceActivity
import com.example.myapplication.R
import com.example.myapplication.adapter.ListMenuAdapter
import com.example.myapplication.data.*
import com.example.myapplication.databinding.FragmentMenuBinding
import com.example.myapplication.model.Food
import com.example.myapplication.model.PostTransaction
import com.example.myapplication.model.PostTransactionItem
import com.google.gson.Gson
import java.text.DecimalFormat
import kotlin.concurrent.thread

class MenuFragment : Fragment() {
    private var menu : ArrayList<Food> = ArrayList()
    private var menuFiltered : ArrayList<Food> = ArrayList()


    private lateinit var binding : FragmentMenuBinding
    private lateinit var  adapter : ListMenuAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMenuBinding.inflate(layoutInflater)
        adapter = ListMenuAdapter(this, menu, menuFiltered, binding.txtTotal)


        binding.searchView.setOnClickListener {
            binding.searchView.requestFocus()
        }

        binding.searchView.setOnQueryTextListener(object  : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                filterMenu(newText)
                return  true
            }

            private fun filterMenu(text: String ) {
                menuFiltered.clear()
                menuFiltered.addAll(menu.filter { it.foodName.lowercase().startsWith(text.lowercase())  })
                binding.listMenu.adapter?.notifyDataSetChanged()
            }

        })

        getMenu()

        binding.btnBayar.setOnClickListener {
            saveTransaksi()
        }

        return binding.root
    }

    fun saveTransaksi() {
        if (menu.filter { it.qty > 0 }.size == 0) {
            Toast.makeText(requireActivity(), "Tidak ada menu yang dipilih", Toast.LENGTH_SHORT)
                .show()
            return
        }
        Keranjang.dataList.clear()
        Keranjang.dataList.addAll(menu.filter { it.qty > 0 })
        startActivity(Intent(requireActivity(), ConfirmationActivity::class.java))
    }

    fun getMenu()  {
        thread {
        try {
            val data = Connect.getProducts()


            if (data.size > 0) {
                Log.d("data-1",data.toString())
                menu.clear()
                menuFiltered.clear()
                menu.addAll(data)
                menuFiltered.addAll(menu)

                activity?.runOnUiThread {
                    binding.listMenu.adapter = adapter
                }
            } else {
                activity?.runOnUiThread {
                    Toast.makeText(activity, "Tidak ada data", Toast.LENGTH_SHORT).show()
                }
            }
        } catch (ex : Exception) {
            Log.d("err-getMenu", ex.toString())
        }
        }

    }
}



