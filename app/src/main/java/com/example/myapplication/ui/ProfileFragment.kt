package com.example.myapplication.ui

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.myapplication.R
import com.example.myapplication.data.Connect
import com.example.myapplication.databinding.FragmentProfileBinding
import kotlin.concurrent.thread

class ProfileFragment : Fragment() {
    private lateinit var binding : FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProfileBinding.inflate(layoutInflater)

        getProfile()

        return binding.root
    }

    fun getProfile () {
        thread {
            val profile = Connect.getProfile()
            if (profile != null) {

            activity?.runOnUiThread {
                binding.txtNamaMenu.text = profile.fullName
                    binding.txtTelpon.text = profile.username
                binding.txtAlamat.text = profile.address

            }
            } else {
                activity?.runOnUiThread {
                    Toast.makeText(activity, "Get Profile failed", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }


}