package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.myapplication.data.Connect
import com.example.myapplication.databinding.ActivityLoginBinding
import com.example.myapplication.model.LoginRequest
import kotlin.concurrent.thread

class LoginActivity : AppCompatActivity() {
    private lateinit var binding : ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            val username = binding.edtUsername.text.toString()
            val password = binding.edtPassword.text.toString()
            thread {
                try {
                    val status = Connect.login(LoginRequest(password,  username), this)
                    if (status) {
                        startActivity(Intent(this, HomeActivity::class.java))
                    } else {
                        runOnUiThread {
                            Toast.makeText(this, "Username atau password salah", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                } catch (ex : Exception) {
                    Log.d("err-login", ex.toString())
                }
            }
        }

        binding.btnRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            binding.edtUsername.setText("")
            binding.edtPassword.setText("")
        }
    }
}