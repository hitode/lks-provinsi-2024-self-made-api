package com.example.myapplication

import android.app.Activity
import android.content.Intent
import android.icu.text.DecimalFormat
import android.os.Build
import android.os.Bundle
import android.renderscript.Script.KernelID
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.myapplication.adapter.ListInvoiceAdapter
import com.example.myapplication.data.Connect
import com.example.myapplication.data.Keranjang
import com.example.myapplication.databinding.ActivityConfirmationBinding
import com.example.myapplication.model.Customer
import com.example.myapplication.model.PostTransaction
import com.example.myapplication.model.PostTransactionItem
import com.google.gson.Gson
import kotlin.concurrent.thread

class ConfirmationActivity : AppCompatActivity() {
    private var listCustomer : ArrayList<Customer> = arrayListOf()
    private lateinit var  adapter : ListInvoiceAdapter
    private lateinit var binding : ActivityConfirmationBinding
    val menu = Keranjang.dataList
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityConfirmationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupCustoemr()

        adapter = ListInvoiceAdapter(Keranjang.dataList)
        binding.listMenu.adapter = adapter
        adapter.notifyDataSetChanged()
        binding.txtTotal.setText("Total : Rp. " + DecimalFormat("#,###").format(Keranjang.dataList.sumOf { it.qty * it.price }))


        binding.btnBayar.setOnClickListener {
            if (binding.spinnerCustomer.selectedItemPosition == -1) {
                Toast.makeText(this, "Tidak ada customer yang dipilih", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val customerId = listCustomer.get(binding.spinnerCustomer.selectedItemPosition).id
            thread {
                try {
                    val total = menu.filter { it.qty > 0 }.sumOf { it.qty * it.price }

                    val items : ArrayList<PostTransactionItem>  = ArrayList()
                    val temp = menu.filter { it.qty > 0 }

                    for (i in 0 .. temp.size - 1) {
                        items.add(
                            PostTransactionItem(
                                temp.get(i).id.toString(),
                                temp.get(i).qty.toString(),
                            )
                        )
                    }
                    val post = PostTransaction(
                        customerId.toString(),
                        items,
                        total.toString(),
                    )
                    val gson = Gson()
                    Log.d("my-json", gson.toJson(post).toString())
                    val status = Connect.saveTransaksi(post)
                    if (status) {
                        runOnUiThread {
                            Toast.makeText(this, "Berhasil melakukan transaksi", Toast.LENGTH_SHORT)
                                .show()
                            startActivity(Intent(this, InvoiceActivity::class.java))
                        }
                    } else {
                        runOnUiThread {
                            Toast.makeText(this, "Gagal melakukan transaksi", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                } catch (ex : Exception) {
                    Log.d("err-saveTransaksi", ex.toString())
                }
            }
        }
    }

    fun setupCustoemr() {
        thread {
            val data = Connect.getCustomer()

            val dataCustomer : ArrayList<String> = arrayListOf()
            listCustomer.clear()
        listCustomer.addAll(data)
            listCustomer.forEach {
                dataCustomer.add(it.name)
            }
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, dataCustomer)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            runOnUiThread {
                binding.spinnerCustomer.adapter = arrayAdapter
            }
        }
    }
}