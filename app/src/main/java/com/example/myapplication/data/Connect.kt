package com.example.myapplication.data

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import com.example.myapplication.model.Customer
import com.example.myapplication.model.Food
import com.example.myapplication.model.InvoiceItem
import com.example.myapplication.model.LoginRequest
import com.example.myapplication.model.PostTransaction
import com.example.myapplication.model.Profile
import com.example.myapplication.model.RegisterRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class Connect {
    companion object {
        val url = "http://192.168.127.124:8000"
        var token = ""

        fun login(loginRequest :LoginRequest, activit : Activity) : Boolean {
            try {
                val url_link = URL("${url}/api/login")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "POST"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Content-Type", "application/application/json")

                    val gson = Gson()
                    with(outputStream.bufferedWriter()) {
                        write(gson.toJson(loginRequest).toString())
                        flush()
                    }

                    when(responseCode) {
                        200 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            token = response["token"].toString()
                            activit.getSharedPreferences("login_session", MODE_PRIVATE)
                                .edit()
                                .putString("token", token)
                                .apply()

                            return  true
                        } else ->  {
                            val response = errorStream.bufferedReader().readText()
                            return false
                        }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun register(registerRequest: RegisterRequest): Boolean {
            try {
                val url_link = URL("${url}/api/register")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "POST"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Content-Type", "application/json")

                    val gson = Gson()
                    with(outputStream.bufferedWriter()) {
                        write(gson.toJson(registerRequest).toString())
                        flush()
                    }

                    when(responseCode) {
                       in 200.. 299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            return true
                        }
                        else -> {
                            val response = errorStream.bufferedReader().readText()
                            return false
                        }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun getProducts() : ArrayList<Food> {
            try {
                val url_link = URL("${url}/api/foods")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
                    setRequestProperty("Authorization", "Bearer " + token)
                    val menu : ArrayList<Food> = ArrayList()

                    when(responseCode) {
                        in 200..299 -> {
                            val response = JSONObject(inputStream.bufferedReader().readText())
                            val data = response.getJSONArray("data")
                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<Food>>() {}.type
                            menu.addAll(gson.fromJson(data.toString(), type))
                            return menu
                        } else -> {
                            val response = errorStream.bufferedReader().readText()
                        return menu
                        }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun saveTransaksi(post: PostTransaction): Boolean {
            try {
                val url_link = URL("${url}/api/transaction")

                with(url_link.openConnection() as HttpURLConnection) {
                    requestMethod = "POST"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Content-Type", "application/json")
                    setRequestProperty("Authorization", "Bearer " + token)

                    val gson = Gson()
                    val json = gson.toJson(post)
                    with(outputStream.bufferedWriter()) {
                        write(json.toString())
                        flush()
                    }

                    when(responseCode) {
                        in 200..299 -> {
                            val response = inputStream.bufferedReader().readText().toString()
                            return true
                        } else -> {
                            val response = errorStream.bufferedReader().readText()
                            return false
                        }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

         fun getTransaksi() : ArrayList<InvoiceItem> {
            try {
                val url_link = URL("${url}/api/transaction")

                with(url_link.openConnection() as HttpURLConnection ) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Authorization", "Bearer " + token)

                    var transactions : ArrayList<InvoiceItem> = ArrayList()
                   when(responseCode) {
                       in 200..299 -> {
                           var response = JSONObject(inputStream.bufferedReader().readText())
                           var data = response.getJSONArray("data")
                           val gson = Gson()
                           val type = object  : TypeToken<ArrayList<InvoiceItem>>() {}.type
                           transactions.addAll(gson.fromJson(data.toString(), type))
                           return transactions
                       } else -> {
                           val response = errorStream.bufferedReader().readText()
                           return transactions
                       }
                   }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun getProfile() : Profile? {
            try {
                val url_link = URL("${url}/api/user")

                with(url_link.openConnection() as HttpURLConnection ) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Authorization", "Bearer " + token)

                    when(responseCode) {
                        in 200..299 -> {
                            var response = JSONObject(inputStream.bufferedReader().readText())
                            val gson = Gson()
                            val data = gson.fromJson<Profile>(response.getJSONObject("data").toString(), Profile::class.java)
                            return data
                        } else -> {
                        val response = errorStream.bufferedReader().readText()
                        return null
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }

        fun getCustomer() : ArrayList<Customer> {
            try {
                val url_link = URL("${url}/api/customer")

                with(url_link.openConnection() as HttpURLConnection ) {
                    requestMethod = "GET"
                    setRequestProperty("Accept", "application/json")
                    setRequestProperty("Authorization", "Bearer " + token)

                    val list : ArrayList<Customer> = arrayListOf()
                    when(responseCode) {
                        in 200..299 -> {
                            var response = JSONObject(inputStream.bufferedReader().readText())
                            val gson = Gson()
                            val type = object : TypeToken<ArrayList<Customer>>() {}.type
                            val data = gson.fromJson<ArrayList<Customer>>(response.getJSONArray("data").toString(), type)
                            list.addAll(data)
                            return  list
                        } else -> {
                        val response = errorStream.bufferedReader().readText()
                        return list
                    }
                    }
                }
            } catch (ex : Exception) {
                throw ex
            }
        }


//        fun makeJson(post: EntityPostTransaction): String {
//            var json = ""
//            for (i in 0 .. post.items.size - 1) {
//                val x = post.items.get(i)
//                json += "{\"foodId\" : ${x.id}, \"quantitas\" : ${x.qty}},"
//            }
//            json = "{\"userId\" : ${post.userId}, \"total\" : ${post.total}, \"foods\" : [${json.substring(0, json.length - 1)}]}"
//            return json
//        }

    }
}