package com.example.myapplication

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.example.myapplication.adapter.ListInvoiceAdapter
import com.example.myapplication.data.Keranjang
import com.example.myapplication.databinding.ActivityInvoiceBinding
import com.example.myapplication.model.Food
import java.io.File
import java.io.IOException
import java.text.DecimalFormat
import java.time.LocalDateTime

class InvoiceActivity : AppCompatActivity() {


    val launcher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            // Permission Accepted: Do something
            Log.d("ExampleScreen","PERMISSION GRANTED")

        } else {
            // Permission Denied: Do something
            Log.d("ExampleScreen","PERMISSION DENIED")
        }
    }
    private lateinit var binding : ActivityInvoiceBinding
    @RequiresApi(Build.VERSION_CODES.O)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInvoiceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showToList(Keranjang.dataList)


        binding.btnSelesai.setOnClickListener {
            finish()
        }


        binding.btnSave.setOnClickListener {
//            when (PackageManager.PERMISSION_GRANTED) {
//                ContextCompat.checkSelfPermission(
//                    applicationContext,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE
//                ) -> {
//                    // Some works that require permission
//                    Log.d("ExampleScreen","Code requires permission")
//                }
//                else -> {
//                    // Asking for permission
//                    launcher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                }
//            }
            try {
                val bitmap = bitmapToView(binding.invoicePng)
                val file = bitmapToFile(bitmap, LocalDateTime.now().toString())
                startActivity(Intent().apply {
                    action = Intent.ACTION_VIEW
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    setDataAndType(FileProvider.getUriForFile(this@InvoiceActivity, "MyApplication.provide", file), "image/*")
                })
            } catch (ex : Exception) {
                Log.d("err-save", ex.toString())
            }
        }

        binding.txtTotalHarga.text = "Rp. " + DecimalFormat("#,###").format(Keranjang.dataList.sumOf { it.price * it.qty })

        binding.btnShare.setOnClickListener {
            try {
                val bitmap = bitmapToView(binding.invoicePng)

                val file = bitmapToFile(bitmap, LocalDateTime.now().toString())
                startActivity(Intent().apply {
                    action = Intent.ACTION_SEND
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    putExtra(Intent.EXTRA_STREAM,FileProvider.getUriForFile(this@InvoiceActivity, "MyApplication.provide", file))
                    type = "image/*"
                })
            } catch (ex : Exception) {
                Log.d("err-save", ex.toString())
            }
        }
    }

    fun showToList(data : List<Food>) {
            try {
                binding.listInvoice.adapter = ListInvoiceAdapter( data)
            } catch (ex : Exception) {
                Log.d("err-show", ex.toString())
            }
    }

    fun bitmapToView(view : View) : Bitmap{
        var bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canva = Canvas(bitmap)
        view.draw(canva)
        return bitmap
    }

    fun bitmapToFile(bitmap: Bitmap, fileName : String) : File {
        val file = File(getExternalFilesDir(Environment.getExternalStorageDirectory().toString()), fileName + ".jpeg")
        try {
            file.createNewFile()
            file.outputStream().run {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, this)
                flush()
                close()
            }
            Toast.makeText(this, file.path, Toast.LENGTH_SHORT).show()
            return file
        } catch (ex : IOException ) {
            throw ex
        }
    }
}

