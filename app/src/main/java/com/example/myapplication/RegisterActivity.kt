package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.myapplication.data.Connect
import com.example.myapplication.databinding.ActivityRegisterBinding
import com.example.myapplication.model.RegisterRequest
import kotlin.concurrent.thread

class RegisterActivity : AppCompatActivity() {
    private  lateinit var binding : ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegister.setOnClickListener {
            val nama = binding.edtNama.text.toString()
            val username = binding.edtUsername.text.toString()
            val password = binding.edtPassword.text.toString()
            val alamat = binding.edtAlamat.text.toString()
            val passwordConfirm = binding.edtPasswordConfirm.text.toString()

            if (nama.isNullOrEmpty() || username.isNullOrEmpty() || password.isNullOrEmpty() || alamat.isNullOrEmpty() || passwordConfirm.isNullOrEmpty()) {
                Toast.makeText(this, "Lengkapi semua data", Toast.LENGTH_SHORT).show()
            } else {
                if (password != passwordConfirm) {
                    Toast.makeText(this@RegisterActivity, "Password tidak sama", Toast.LENGTH_SHORT)
                        .show()
                    return@setOnClickListener
                }
                thread {
                    try {
                        val status = Connect.register(RegisterRequest(alamat, nama, password, passwordConfirm, username))

                        if (status) {
                            finish()
                            runOnUiThread {
                                Toast.makeText(this, "Registrasi berhasil", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        } else {
                            runOnUiThread {
                                Toast.makeText(this, "Registrasi gagal", Toast.LENGTH_SHORT).show()
                            }
                        }
                    } catch (ex : Exception) {
                        Log.d("err-register", ex.toString())
                    }
                }
            }
        }
    }
}