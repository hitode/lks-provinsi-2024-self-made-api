package com.example.myapplication.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostTransactionItem(
    val foodId: String,
    val quantitas: String
) : Parcelable