package com.example.myapplication.model

data class InvoiceItem(
    val created_at: String,
    val customer_id: Int,
    val detail_transaction: Any,
    val id: Int,
    val invoiceNumber: String,
    val total: Int,
    val updated_at: String,
    val user_id: Int
)