package com.example.myapplication.model

data class Food(
    val created_at: String,
    val foodName: String,
    val id: Int,
    val image: String,
    val price: Int,
    val ratings: Double,
    val stock: Int,
    val updated_at: String,
    var qty : Int = 0
)