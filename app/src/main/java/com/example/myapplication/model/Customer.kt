package com.example.myapplication.model

data class Customer(
    val city: String,
    val contact_number: String,
    val created_at: String,
    val id: Int,
    val image: String,
    val name: String,
    val updated_at: String
)