package com.example.myapplication.model

data class Profile(
    val address: String,
    val created_at: String,
    val fullName: String,
    val id: Int,
    val updated_at: String,
    val username: String
)