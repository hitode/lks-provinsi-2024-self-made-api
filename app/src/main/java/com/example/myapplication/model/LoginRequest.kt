package com.example.myapplication.model

data class LoginRequest(
    val password: String,
    val username: String
)