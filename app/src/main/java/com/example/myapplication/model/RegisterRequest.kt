package com.example.myapplication.model

data class RegisterRequest(
    val address: String,
    val fullName: String,
    val password: String,
    val password_confirmation: String,
    val username: String
)