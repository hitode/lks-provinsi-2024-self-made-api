package com.example.myapplication.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostTransaction(
    val customer_id: String,
    val foods: List<PostTransactionItem>,
    val subTotal: String
) : Parcelable