package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.InvoiceActivity
import com.example.myapplication.R
import com.example.myapplication.databinding.ItemInvoiceBinding
import com.example.myapplication.model.Food
import java.text.DecimalFormat

class ListInvoiceAdapter( val data: List<Food>) :
    RecyclerView.Adapter<ListInvoiceAdapter.ListInvoiceHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListInvoiceHolder {
        return ListInvoiceHolder(
            ItemInvoiceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ListInvoiceHolder, position: Int) {
        var x = data.get(position)

        with(holder) {
            binding.txtNamaMenu.text = x.foodName
            binding.txtHarga.text = "Rp. " + DecimalFormat("#,###").format(x.price)
            binding.txtQty.text = x.qty.toString()
            binding.txtSubTotal.text = "Rp. " + DecimalFormat("#,###").format((x.price * x.qty))
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ListInvoiceHolder(val binding : ItemInvoiceBinding) : RecyclerView.ViewHolder(binding.root) {
    }

}
