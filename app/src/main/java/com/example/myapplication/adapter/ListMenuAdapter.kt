package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.data.Connect
import com.example.myapplication.databinding.ItemMenuBinding
import com.example.myapplication.model.Food
import com.example.myapplication.ui.MenuFragment
import java.text.DecimalFormat


class ListMenuAdapter(
    val menuFragment: MenuFragment,
    val menu: java.util.ArrayList<Food>,
    val menuFiltered: java.util.ArrayList<Food>,
    val txtTotal: TextView
) : RecyclerView.Adapter<ListMenuAdapter.ListMenuHolder>() {

    class ListMenuHolder(val binding  : ItemMenuBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListMenuHolder {
        return ListMenuHolder(
                ItemMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ListMenuHolder, position: Int) {
        var data = menuFiltered.get(position)
        var dataMenu = menu.find { it.id == data.id }

        with(holder) {
            binding.txtNamaMenu.text = data.foodName
            if (dataMenu != null) {
                binding.txtQty.text = dataMenu.qty.toString()
                binding.txtPrice.text = "Rp. " + DecimalFormat("#,###").format(data.price)
            }

            binding.btnAdd.setOnClickListener {
                if (dataMenu != null) {
                    dataMenu.qty++
                    binding.txtQty.text = dataMenu.qty.toString()
                    sumTotal(txtTotal, menu)
                }
            }

            binding.btnRemoveQty.setOnClickListener {
                if (dataMenu != null && dataMenu.qty > 0) {
                    dataMenu.qty--
                    binding.txtQty.text = dataMenu.qty.toString()
                    sumTotal(txtTotal, menu)
                }
            }
            Glide.with(menuFragment)
                .load(("${Connect.url}/${dataMenu?.image}"))
                .into(binding.imgMenu)
        }
    }

    private fun sumTotal(txtTotal: TextView, dataMenu : ArrayList<Food>) {
        var total =dataMenu.sumOf { it.qty * it.price }

        txtTotal.text = "Rp. " + DecimalFormat("#,###").format(total)
    }

    override fun getItemCount(): Int {
        return menuFiltered.size
    }
}

