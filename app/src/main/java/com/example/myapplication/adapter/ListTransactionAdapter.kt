package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.databinding.ItemTransactionBinding
import com.example.myapplication.model.InvoiceItem
import com.example.myapplication.ui.TransactionFragment
import java.text.DecimalFormat

class ListTransactionAdapter(val transactionFragment: TransactionFragment, val data: ArrayList<InvoiceItem>) : RecyclerView.Adapter<ListTransactionAdapter.ListTransactionHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTransactionHolder {
        return ListTransactionHolder(
            ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ListTransactionHolder, position: Int) {
        val x = data.get(position)

        with(holder) {
            binding.txtNoTransaksi.text = x.invoiceNumber
            binding.txtTglTransaksi.text = x.created_at
            binding.txtTotalHarga.text = "Rp. " + DecimalFormat("#,###").format(x.total)
//            txtProduk.text = "Jumlah produk : ${x.detail_transaction.size}"
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }
    class ListTransactionHolder(val binding : ItemTransactionBinding) : RecyclerView.ViewHolder(binding.root) {
    }
}


