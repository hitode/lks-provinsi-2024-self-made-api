package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val nav_con = findViewById<BottomNavigationView>(R.id.nav_bottom)
        val nav_host = supportFragmentManager.findFragmentById(R.id.nav_host) as NavHostFragment
        val navController = nav_host.navController
        nav_con.setupWithNavController(navController)
    }
}